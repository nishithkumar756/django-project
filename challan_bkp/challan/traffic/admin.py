from django.contrib import admin
from .models import Challan, Report

admin.site.register(Challan)
admin.site.register(Report)
