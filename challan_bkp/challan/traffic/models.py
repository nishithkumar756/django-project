from django.db import models

# Create your models here.
from django.db import models
from datetime import datetime,timedelta
from django.contrib.auth.models import User





def default_start_time():
    now = datetime.now()
    start = now.replace(hour=22, minute=0, second=0, microsecond=0)
    return start if start > now else start + timedelta(days=1)  

class Challan(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE,default=None)
   
    place_of_violation=models.CharField(max_length=100,default=None)
    date=models.DateField(default=datetime.now)
    time=models.TimeField(default=default_start_time)
    cat=(('Wrong Route','Wrong Route'),('Drunk and Drive','Drunk and Drive'),('No helmet','No helmet'),('Parked at No parking','Parked at No parking'),('Overspeed','Overspeed'),('Signal Jump','Signal Jump'))
    violation=models.CharField(max_length=50,choices=cat)
    fine_amount=models.DecimalField(max_digits=10000,decimal_places=2,default=None)


class Report(models.Model):
    registration_number=models.TextField(default=None)
    phone=models.IntegerField(default=None)
    compliant=models.TextField(default=None)








    
