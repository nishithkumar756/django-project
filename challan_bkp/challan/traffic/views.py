
from django.shortcuts import render,  redirect,get_object_or_404
from .models import Challan, Report
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.db.models import Count
from django.contrib import messages
from django.db.models import Sum
import re
import razorpay


from django.contrib.auth.models import Group
from django.contrib.auth.models import User


def home_view(request):
    challans = Challan.objects.all()
    return render(request, 'traffic/home.html',{challans:"challans"})

def user_login_view(request):
    if request.method == 'POST':
        username_ = request.POST.get('vehicle_number', '')
        password_ = request.POST.get('password', '')
        print(username_)
        print(password_)
        
        user = authenticate(request,username=username_, password=password_)
        if user is not None :
            login(request, user)

            users = Group.objects.get(name='Users')
            
            if users in user.groups.all():
                
                return redirect('traffic:view_challans')
            
        messages.error(request, 'Invalid credentials. Please try again.')

    return render(request,'traffic/user_login.html' )

@login_required
def report(request):
    if request.method == 'POST':
        registration_number = request.POST.get('registration_number')
        phone = request.POST.get('phone')
        complaint = request.POST.get('complaint')


        report = Report(registration_number=registration_number, phone=phone, compliant=complaint)
        report.save()
        messages.success(request, 'Your complaint is registered')
       
        return render(request,'traffic/report.html')
    else:
        
        return render(request,'traffic/report.html')

def register_page(request):
    if request.method == 'GET':
        return render(request, 'traffic/register.html')
    elif request.method == 'POST':
        registration_number_ = request.POST.get('registration_number', '')
        name_ = request.POST.get('name', '')
        phone_ = request.POST.get('phone', '')
        password_ = request.POST.get('password', '')
        confirm_password = request.POST.get('confirm_password', '')

        if password_ == confirm_password:
            if registration_number_ == '':
                messages.error(request, 'Please enter your registration number')
                return redirect('traffic:register')
            elif name_ == '':
                messages.error(request, 'Please enter your name')
                return redirect('traffic:register')
            elif password_ == '':
                messages.error(request, 'Please enter your password')
                return redirect('traffic:register')
            elif confirm_password == '':
                messages.error(request, 'Please enter your confirm password')
                return redirect('traffic:register')
            elif phone_ == '':
                messages.error(request, 'Please enter your phone number')
                return redirect('traffic:register')

            elif User.objects.filter(username=registration_number_).exists():
                messages.error(request, 'Registration number already exists')
                return redirect('traffic:register')
            elif User.objects.filter(last_name=phone_).exists():
                messages.error(request, 'Phone number already exists')
                return redirect('traffic:register')
            elif len(phone_) != 10:
                messages.error(request, 'Phone number must contain ten digits')
                return redirect('traffic:register')
            elif not re.match(r'^[A-Z]{2}\d{2}[A-Z]{2}\d{4}$', registration_number_):
                messages.error(request, 'Registration number should be in the format "TS09FC2342"')
                return redirect('traffic:register')
            else:
                user = User.objects.create_user(username=registration_number_, password=password_)
                user.first_name = name_
                user.last_name = phone_
                user.save()
                user_group = Group.objects.get(name='Users')
                user.groups.add(user_group)
                context = {'msg': 'Registration Successful'}
                return render(request, 'traffic/error.html', context)
        else:
            messages.error(request, 'Make sure both passwords are the same')
            return redirect('traffic:register')

def contact(request):
    return render(request, 'traffic/contact_us.html') 

def traffic_rules(request):
    return render(request, "traffic/traffic_rules.html")

def user_logout_view(request):
    logout(request)
    return redirect('traffic:user_login')           

@login_required
def view_challans(request):
    user = request.user
    challans = Challan.objects.filter(user=user)
    return render(request, 'traffic/challan_view.html', {'challans': challans})

@login_required
def report_list(request):
    reports = Report.objects.all()
    return render(request, 'traffic/report_list.html', {'reports': reports})

@login_required
def police_dashboard(request):
     return render(request, 'traffic/police_dashboard.html')
@login_required
def police_dashboard_content(request):
    return render(request,'traffic/police_dashboard_content.html')

@login_required
def view_challans(request):
    user = request.user
    challans = Challan.objects.filter(user=user)

    if request.method == 'POST':
        challan_ids = request.POST.getlist('challan_ids')
        challans = Challan.objects.filter(id__in=challan_ids, user=user)
        total_amount = challans.aggregate(total=Sum('fine_amount'))['total']
        total_amount_paise = total_amount * 100
   
    

        return render(request, 'traffic/pay_challans.html', {'challans': challans, 'total_amount': total_amount, 'total_amount_paise': total_amount_paise})

    return render(request, 'traffic/challan_view.html', {'challans': challans})

@login_required
def generate(request):
    if request.method == 'POST':
       
        user_id = request.POST.get('user')
        user = User.objects.get(id=user_id)
        place_of_violation_ = request.POST.get('place_of_violation')
        date_ = request.POST.get('date')
        time_ = request.POST.get('time')
        violation_type = request.POST.get('violation_type')
        fine_amount_ = request.POST.get('fine_amount')

        try:
          

            challan = Challan(user=user,place_of_violation= place_of_violation_,date=date_ ,time=time_,violation=violation_type, fine_amount=fine_amount_)
            challan.save()
            

            messages.success(request, 'Challan Successfully Generated')
            return redirect('traffic:generate')
        except Challan.DoesNotExist:
            messages.error(request, 'Failed to generate challan')
            return redirect('traffic:generate')
        

    users = User.objects.filter(groups__name='Users')
    return render(request, "traffic/generate.html",{'users': users})

@login_required
def admin_generate(request):
    if request.method == 'POST':
       
        user_id = request.POST.get('user')
        user = User.objects.get(id=user_id)
        place_of_violation_ = request.POST.get('place_of_violation')
        date_ = request.POST.get('date')
        time_ = request.POST.get('time')
        violation_type = request.POST.get('violation_type')
        fine_amount_ = request.POST.get('fine_amount')

        try:
          

            challan = Challan(user=user,place_of_violation= place_of_violation_,date=date_ ,time=time_,violation=violation_type, fine_amount=fine_amount_)
            challan.save()
            

            messages.success(request, 'Challan Successfully Generated')
            return redirect('traffic:admin_generate')
        except Challan.DoesNotExist:
            messages.error(request, 'Failed to generate challan')
            return redirect('traffic:admin_generate')
        

    users = User.objects.filter(groups__name='Users')
    return render(request, "traffic/admin_generate.html",{'users': users})


@login_required
def pending_challan(request):
    challans = Challan.objects.all()
    return render(request, 'traffic/pending_challan.html', {'challans': challans})


def traffic_rules(request):
    return render(request, "traffic/traffic_rules.html")




@login_required
def admin_display_challans(request):
    challans = Challan.objects.all()
    return render(request, 'traffic/admin_challan_cancellation.html', {'challans': challans})

@login_required
def admin_confirm_delete_challan(request, challan_id):
    challan = get_object_or_404(Challan, id=challan_id)
    return render(request, 'traffic/admin_confirm_delete.html', {'challan': challan})



@login_required
def admin_confirm_delete_complaint(request, report_id):
    report = get_object_or_404(Report, id=report_id)
    return render(request, 'traffic/admin_confirm_delete_complaint.html', {'report': report})

@login_required
def admin_dashboard_content(request):
    return render(request,'traffic/admin_content.html')


def admin_login_view(request):
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        password_ = request.POST.get('password', '')
        user = authenticate(request, username=username_, password=password_)
        
        if user is not None and user.is_superuser :
            
            
            if user.groups.filter(name='Admin').exists():
                login(request, user)
                return redirect('traffic:admin_dashboard_content')
            
        messages.error(request, 'Invalid credentials. Please try again.')

    return render(request, 'traffic/admin_login.html')



@login_required
def admin_report_list(request):
    reports = Report.objects.all()
    return render(request, 'traffic/admin_report_list.html', {'reports': reports}) 


@login_required
def all_users_view(request):
    users = User.objects.filter(groups__name='Users')
    police = User.objects.filter(groups__name='Police')
    return render(request, 'traffic/all_users_view.html', {'users': users,'police':police})


@login_required
def admin_panel_view(request):
    if request.method == 'POST':
        user_group = request.POST.get('user_group')
        username = request.POST.get('username')

        try:
            user = User.objects.get(username=username)
            group = Group.objects.get(name=user_group)
            user.groups.remove(group)
            user.delete()
            messages.success(request, f'Successfully removed user {username} from group {user_group}')
        except User.DoesNotExist:
            messages.error(request, f'User {username} does not exist')
        except Group.DoesNotExist:
            messages.error(request, f'Group {user_group} does not exist')

    users_group_users = User.objects.filter(groups__name='Users')
    users_group_police = User.objects.filter(groups__name='Police')

    return render(request, 'traffic/admin.html', {
        'users_group_users': users_group_users,
        'users_group_police': users_group_police,
    })        
    

def admin_logout_view(request):
    logout(request)
    return redirect('traffic:admin_login')


@login_required
def display_challans(request):
    challans = Challan.objects.all()
    return render(request, 'traffic/challan_cancellation.html', {'challans': challans}) 



@login_required
def ViolationsByLocationView(request):
    violations = Challan.objects.values( 'place_of_violation').annotate(total=Count('id')).order_by('-total')[:10]
    vio=Challan.objects.values('violation').annotate(total=Count('id')).order_by('-total')[:10]
    context = {'violations': violations , 'vio':vio}
    return render(request, 'traffic/most_violations.html', context)

@login_required
def pending_challan(request):
    challans = Challan.objects.all()
    return render(request, 'traffic/pending_challan.html', {'challans': challans})


@login_required
def confirm_delete_complaint(request, report_id):
    report = get_object_or_404(Report, id=report_id)
    return render(request, 'traffic/confirm_delete_complaint.html', {'report': report})


@login_required
def confirm_delete_challan(request, challan_id):
    challan = get_object_or_404(Challan, id=challan_id)
    return render(request, 'traffic/confirm_delete.html', {'challan': challan})

def faq(request):
    return render(request, "traffic/faq's.html")   


def police_logout_view(request):
    logout(request)
    return redirect('traffic:police_login')

@login_required
def delete_challan(request, challan_id):
    
        try:
            challan = Challan.objects.get(id=challan_id)
            challan.delete()
        except Challan.DoesNotExist:
            pass
        return redirect('traffic:display_challans') 


@login_required
def admin_delete_challan(request, challan_id):
    
        try:
            challan = Challan.objects.get(id=challan_id)
            challan.delete()
        except Challan.DoesNotExist:
            pass
        return redirect('traffic:admin_display_challans') 
         

@login_required
def delete_complaint(request, report_id):
    
        try:
            report = Report.objects.get(id=report_id)
            report.delete()
        except Report.DoesNotExist:
            pass
        return redirect('traffic:report_list')


@login_required
def admin_delete_complaint(request, report_id):
    
        try:
            report = Report.objects.get(id=report_id)
            report.delete()
        except Report.DoesNotExist:
            pass
        return redirect('traffic:admin_report_list')


def police_login_view(request):

    if request.method == 'POST':
        username_ = request.POST.get('user_name', '')
        password_ = request.POST.get('pass_word', '')
        user = authenticate(request, username=username_, password=password_)
        if user is not None:
            login(request, user)
            police = Group.objects.get(name='Police')
            
            if police in user.groups.all():
                return redirect('traffic:police_dashboard_content')
            
        messages.error(request, 'Invalid credentials. Please try again.')

    return render(request,'traffic/police_login.html' ) 

def police_register_page(request):
    if request.method == 'GET':
        return render(request, 'traffic/police_register.html')
    if request.method == 'POST':
        name_ = request.POST.get('name', '')
        password_ = request.POST.get('password', '')
        confirm_password = request.POST.get('confirm_password', '')
        phone_=request.POST.get('phone_no', '')
        
        if name_ == '':
            messages.error(request, 'Please enter your name')
            return redirect('traffic:police_register')
        
        if phone_ == '':
            messages.error(request, 'Please enter phone number')
            return redirect('traffic:police_register')

        if password_ == '':
            messages.error(request, 'Please enter the password')
            return redirect('traffic:police_register')

        if confirm_password == '':
            messages.error(request, 'Please enter the confirm password')
            return redirect('traffic:police_register')
        

        if password_ != confirm_password:
            messages.error(request, "Make sure both passwords are the same")
            return render(request, 'traffic/police_register.html')

        if User.objects.filter(username=name_).exists():
            messages.error(request, 'Username already exists')
            return redirect('traffic:police_register')
        
        if User.objects.filter(last_name=phone_).exists():
            messages.error(request, 'Phone Number already exists')
            return redirect('traffic:police_register')
        
        if len(phone_) != 10:
            messages.error(request, 'Phone number must contain ten digits')
            return redirect('traffic:police_registerr')

        if name_ == '':
            messages.error(request, 'Please enter your name')
            return redirect('traffic:police_register')
        
        if phone_ == '':
            messages.error(request, 'Please enter phone number')
            return redirect('traffic:police_register')

        if password_ == '':
            messages.error(request, 'Please enter the password')
            return redirect('traffic:police_register')

        if confirm_password == '':
            messages.error(request, 'Please enter the confirm password')
            return redirect('traffic:police_register')

        user = User.objects.create(username=name_)
        user.set_password(password_)
        user.last_name = phone_
        user.save()

        context = {'msg': "Registration Successful"}

        
        police_group = Group.objects.get(name='Police')
        user.groups.add(police_group)
        return render(request, 'traffic/error.html', context)
    

    



@login_required
def pay_challans(request):
    if request.method == 'POST':

        

        challan_ids = request.POST.getlist('challan_ids')
        challans = Challan.objects.filter(id__in=challan_ids, user=request.user)
        total_amount = challans.aggregate(total=Sum('fine_amount'))['total']
        total_amount_paise = total_amount * 100

        client = razorpay.Client(auth=("rzp_test_rvLIJv4Y9azQt8", "qqZXRXfFZpvDN5OwrsKgEfzT "))

        DATA = {
            "amount": 100,
            "currency": "INR",
           
            
        } 
        payment= client.order.create(data=DATA)

        paid_challans = Challan.objects.filter(id__in=challan_ids, user=request.user)
        paid_challans.update(paid=True)
        return render(request, 'traffic/pay_challans.html', {'challans': challans, 'total_amount': total_amount,'payment':payment,'total_amount_paise':total_amount_paise
        })
    return redirect('traffic:view_challans')