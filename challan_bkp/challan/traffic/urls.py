from django.urls import path
from . import views
app_name='traffic'
urlpatterns = [
    path('home/',views.home_view,name='home'),
    path('generate/',views.generate,name="generate"),
    path('admin_generate/',views.admin_generate,name="admin_generate"),
    path('challans/', views.view_challans, name='view_challans'),
    path('rules/',views.traffic_rules,name="rules"),
    path('most_violations/',views.ViolationsByLocationView,name='violation'),
    path('register/',views.register_page,name='register'),
    path('police_register/',views.police_register_page,name='police_register'),
    path('police_login/',views.police_login_view,name='police_login'),
    path('contact/',views.contact,name='contact'),
    path('faq/',views.faq,name='faq'),
    path('user_login/',views.user_login_view,name='user_login'),
    path('user_logout/', views.user_logout_view, name='user_logout'),
    path('police_logout/', views.police_logout_view, name='police_logout'),
    path('admin_logout_view/', views.admin_logout_view, name='admin_logout_view'),
    path('police_dashboard/', views.police_dashboard, name='police_dashboard'),
    path('police_dashboard_content/', views.police_dashboard_content, name='police_dashboard_content'),
    path('admin_dashboard_content/', views.admin_dashboard_content, name='admin_dashboard_content'),
    path('admin_panel/', views.admin_panel_view, name='admin_panel'),
    path('admin_login/',views.admin_login_view,name="admin_login"),
    path('pending_challan/',views.pending_challan,name="pending_challan"),
    path('report/',views.report,name="report"),
    path('report_list/',views.report_list,name="report_list"),
    path('admin_report_list/',views.admin_report_list,name="admin_report_list"),
    path('challan_cancellation/', views.display_challans, name='display_challans'),
    path('admin_challan_cancellation/', views.admin_display_challans, name='admin_display_challans'),
    path('challan_cancellation/delete/<int:challan_id>/', views.delete_challan, name='delete_challan'),
    path('admin_challan_cancellation/delete/<int:challan_id>/', views.admin_delete_challan, name='admin_delete_challan'),
    path('challan_cancellation/delete/confirm-delete/<int:challan_id>/', views.confirm_delete_challan, name='confirm_delete_challan'),
    path('admin_challan_cancellation/delete/confirm-delete/<int:challan_id>/', views.admin_confirm_delete_challan, name='admin_confirm_delete_challan'),
    path('report_list/delete/<int:report_id>/', views.delete_complaint, name='delete_complaint'),
    path('admin_report_list/delete/<int:report_id>/', views.admin_delete_complaint, name='admin_delete_complaint'),
    path('report_list/delete/confirm-delete/<int:report_id>/', views.confirm_delete_complaint, name='confirm_delete_complaint'),
    path('admin_report_list/delete/confirm-delete/<int:report_id>/', views.admin_confirm_delete_complaint, name='admin_confirm_delete_complaint'),
    path('all_users_view/', views.all_users_view, name='all_users_view'),
    path('pay_challans/', views.pay_challans, name='pay_challans'),
    
   
    
    

]

  

   
